import React from 'react';
import {
    View, Text, StatusBar, Alert
} from 'react-native'
import { Button, Icon } from 'react-native-elements'
import styles from './styles';
import { globalStyles, colors } from '../../constants';
import GradientText from '../../components/GradientText';
import PayPal from 'react-native-paypal-wrapper';
import API from '../../api/';

class Checkout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price: 0,
            currency: 'USD',
            execute: false
        }
    }

    componentDidMount = async () => {
        var userid = this.props.tour.posterid
        var _this = this;
        await API.post('/user/get_tour_fee', {
            userid: userid 
        }).then(result => {
            _this.setState({
                price: result.data.tourfeeinfo.tourfee_value,
                currency: result.data.tourfeeinfo.tourfee_currency
            })
        });
    }

    pressCheckout = () => {
        this.setState({ execute: true })
        PayPal.initialize(PayPal.SANDBOX, "ARlu9ZUkC8Hu3oIqq5dUbVu0doucNn4vGxG3I-xEMAdr8kodqeKkNuMac_k9RkDU9dtmfbqq6fm_92ZH");
        PayPal.pay({
            price: this.state.price,
            currency: this.state.currency,
            description: 'Your description goes here',
        }).then(async confirm => {
            console.log(confirm) 
            this.setState({ execute: false });
            await this.props.orderTour(this.props.tour.tourID, confirm.response.create_time);
            this.props.navigation.navigate('MyTours');
        }).catch(error => {
            console.log(error)
            alert(error.error)
            this.setState({ execute: false })
        });
    }

    render() {
        return (
            <View style={{ flex: 1, paddingBottom: 0 }}>
                <StatusBar
                    translucent={true}
                    backgroundColor="transparent"
                    barStyle="light-content"
                />
                <View style={styles.header}>
                    <View style={{ flex: 1, alignITems: 'center', justifyContent: 'center' }}>
                        <Icon name="menu"
                            type="material-community"
                            color={colors.LIGHT_GREEN}
                            underlayColor="transparent"
                            size={32}
                            onPress={() => {
                                this.props.navigation.openDrawer();
                            }}
                        />
                    </View>
                    <View style={{ flex: 6, paddingLeft: 40 }}>
                        <GradientText style={globalStyles.headerTitle}>Checkout</GradientText>
                    </View>
                </View>
                <View style={styles.containerBody}>
                    <View>
                        <Text style={{ fontSize: 25, textAlign: 'center', color: '#fafafa', padding: 20 }}>Tour Price: <Text style={{ fontWeight: 'bold' }}>{this.state.price} CAD</Text></Text>
                        <Button title="Pay Now"
                            loading={this.state.execute ? true : false} onPress={this.pressCheckout} />
                    </View>
                </View>
            </View>

        )
    }
}

export default Checkout